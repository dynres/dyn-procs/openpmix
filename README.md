# Time-X EuroHPC project: Dynamic PMIx Extensions (based on OpenPMIx)

This repository is based on an [openpmix](https://github.com/openpmix/openpmix) fork and provides extensions to support the [Time-X Dynamic MPI Interface](https://gitlab.inria.fr/dynres/dyn-procs/ompi) 

## Installation
 
To setup the dynamic PMIx Extensions with the [Time-X Dynmic MPI Interface](https://gitlab.inria.fr/dynres/dyn-procs/ompi) and the [Time-X Dynamic Runtime Environment](https://gitlab.inria.fr/dynres/dyn-procs/prrte) it is strongly recommended to use the descriptions and setup scripts provided in the following repo:

https://gitlab.inria.fr/dynres/dyn-procs/dyn_procs_setup 
 
For a manual setup, run the following commands:

Generate the configure scripts:
`./autogen.pl`

Run the configure script:
`./configure --prefix=/path/for/pmix/install --enable-python-bindings --disable-werror`

Run make:
`make -j all install`

Also see the HACKING.md file for more information on building and installing PMIx.

## Copyright
This repository is based on a fork of the openpmix repository. For copyright information see the README and README_original.md files. 
